from django.shortcuts import render, redirect
from django.forms import ModelForm
from .models import Task
from django.contrib.auth.decorators import login_required

# Create your views here.


class TaskForm(ModelForm):
    class Meta:
        model = Task
        fields = [
            "name",
            "start_date",
            "due_date",
            "project",
            "assignee",
        ]


@login_required
def create_task(request):
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")
    else:
        form = TaskForm()
        context = {
            "form": form,
        }
    return render(request, "create_task.html", context)


@login_required
def show_task(request):
    tasks = Task.objects.filter(assignee=request.user)
    context = {
        "tasks": tasks,
    }
    return render(request, "show_task.html", context)
